package example.com.melani_1202165279_si40int_pab_modul4;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    ListView listViewProduks;
    DatabaseReference databaseProduk;
    Button btnLoad;
    List<Makanan> produks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnLoad = findViewById(R.id.butnLoad);
        listViewProduks = (ListView) findViewById(R.id.listViewProduks);
        produks = new ArrayList<>();
        databaseProduk = FirebaseDatabase.getInstance().getReference("MenuMakanan");

        btnLoad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                new MyTask().execute();

            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,  TambahActivity.class));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent1 = new Intent(this,LoginActivity.class);
            this.startActivity(intent1);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {

            databaseProduk.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    produks.clear();


                    for (DataSnapshot produkSnapshot : dataSnapshot.getChildren()) {

                        Makanan produk = produkSnapshot.getValue(Makanan.class);
                        produks.add(produk);
                    }

                    ProdukAdapter produkAdapter = new ProdukAdapter(MainActivity.this, produks);
                    listViewProduks.setAdapter( produkAdapter);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "eror : .", databaseError.toException());

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
        }
    }
}
